﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SwipeBall.Entity
{
    public class CameraFollow : MonoBehaviour
    {
        [SerializeField] private Camera mainCamera;
        [SerializeField] private Transform targetTransform;
        [Range(0f,0.9f)]
        [SerializeField] private float viewPortRange = 0.8f;
        private Transform _thisTansform;
        private bool moving = false;

        private void Awake()
        {
            _thisTansform = transform;
        }

        private void Update()
        {
            if(moving)
                return;
            var viewPort = mainCamera.WorldToViewportPoint(targetTransform.position);
            if (viewPort.y > viewPortRange)
            {
                var pos = mainCamera.ViewportToWorldPoint(new Vector3(0f, viewPortRange, 0f));
                var distance = Mathf.Abs(_thisTansform.position.y - pos.y);
                StartCoroutine(MoveUp(distance,1f));
            }
        }

        private IEnumerator MoveUp(float distance,float time)
        {
            moving = true;
            var endTime = Time.time + time;
            var originalposition = _thisTansform.position;
            while (Time.time<= endTime)
            {
                var lerpvalue =1f- Mathf.Clamp01((endTime - Time.time) / time);
                _thisTansform.position = Vector3.Lerp(originalposition, originalposition+Vector3.up*distance, lerpvalue);
                yield return new WaitForEndOfFrame();
            }

            moving = false;
        }
    }
}
