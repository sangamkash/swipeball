﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace SwipeBall.Entity.Manager
{
    public class GameManager : MonoBehaviour
    {
        [Header("Entity Ref")]
        [SerializeField] private Camera mainCamera;
        [SerializeField] private Ball ball;
        [SerializeField] private PlatformManager platformManager;
        [SerializeField] private UIManager uiManager;

        [SerializeField] private Basket basket;
        private void Start()
        {
            basket.OnBasketed = OnCompleted;
            Time.timeScale = 0;
            Time.fixedDeltaTime = 0f;
        }

        private void OnEnable()
        {
            uiManager.OnPlay += Play;
        }

        private void OnDisable()
        {
            uiManager.OnPlay -= Play;
        }

        private void Update()
        {
            if(Time.timeScale == 0)
                return;
            
           if(IsOutOfScreen(ball.thisTranform.position))
               GameOver();
        }

        private bool IsOutOfScreen(Vector3 position)
        {
            var viewPort = mainCamera.WorldToViewportPoint(position);
            return viewPort.x > 1f || viewPort.x < 0 || viewPort.y < 0;
        }
        
        public void GameOver()
        {
            platformManager.gameObject.SetActive(false);
            Time.timeScale = 0;
            Time.fixedDeltaTime = 0f;
            uiManager.UpdateInfo("Game Over");
#if DEBUG_LOG
            Debug.Log("GameOver");
#endif
        }

        public void Play()
        {
            Time.timeScale = 1f;
            Time.fixedDeltaTime = 0.02f;
            platformManager.gameObject.SetActive(true);
            
        }

        public void OnCompleted()
        {
            Time.timeScale = 0;
            Time.fixedDeltaTime = 0f;
            uiManager.UpdateInfo("Level completed");
        }
        
    }
}
