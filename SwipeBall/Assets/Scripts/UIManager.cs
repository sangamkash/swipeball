﻿using System;
using System.Collections.Generic;
using SwipeBall.Constants;
using UnityEngine;
using UnityEngine.UI;
using  TMPro;
using UnityEditor;
using UnityEngine.SceneManagement;

namespace SwipeBall.Entity.Manager
{
    public class UIManager : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI info;
        [SerializeField] private GameObject btnPlay;
        [SerializeField] private GameObject btnRetry;

        public event Action OnPlay;  

        public void Play()
        {
            btnPlay.SetActive(false);
            if (OnPlay != null)
                OnPlay();
        }

        public void Retry()
        {
            info.text = "Loading...";
            btnRetry.SetActive(false);
            SceneManager.LoadScene(GameConstants.Scene_Start);
        }

        public void UpdateInfo(string msg)
        {
            info.text = msg;
            info.gameObject.SetActive(true);
            btnRetry.SetActive(true);
        }

    }
}
