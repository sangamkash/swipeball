﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SwipeBall.Entity
{
    public class SideWall : MonoBehaviour
    {
        [SerializeField] private Camera mainCamera;
        [SerializeField] private bool left;

        private void Start()
        {
            var point = left ? new Vector3(0f,0f,0f) : new Vector3(1f,0f,0f);
            var worldPoint = mainCamera.ViewportToWorldPoint(point);
            var position = new Vector3(worldPoint.x, worldPoint.y, 0);
            transform.position = position;
        }
    }
}
