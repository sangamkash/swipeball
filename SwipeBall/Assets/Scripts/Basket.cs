﻿using System;
using System.Collections;
using System.Collections.Generic;
using SwipeBall.Constants;
using UnityEngine;

public class Basket : MonoBehaviour
{
   public Action OnBasketed;
   private void OnTriggerEnter2D(Collider2D other)
   {
      Debug.Log(other.gameObject.name);
      if (other.gameObject.CompareTag(GameConstants.Tag_Ball))
      {
         OnBasketed?.Invoke();
      }
   }
}
