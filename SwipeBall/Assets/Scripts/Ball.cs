﻿using System;
using SwipeBall.Constants;
using UnityEngine;
using Random = UnityEngine.Random;

namespace SwipeBall.Entity
{
    public class Ball : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer ballRenderer;
        [SerializeField] private Color[] randomColors;
        [HideInInspector] public Transform thisTranform;
        private Rigidbody2D thisRigidbody2D;

        private void Awake()
        {
            thisTranform = transform;
            thisRigidbody2D = GetComponent<Rigidbody2D>();
        }

        private int currentIndex=0;
        private void OnCollisionEnter2D(Collision2D other)
        {
            var randomIndex = ReturnRandom(0, randomColors.Length - 1, currentIndex);
            ballRenderer.color = randomColors[randomIndex];
            {
                thisRigidbody2D.velocity = thisRigidbody2D.velocity.normalized * 10f;
            }
        }
        private int ReturnRandom(int min, int max, int currentNumber)
        {
            if (min == currentNumber)
                return Random.Range(min + 1, max+1);
            else if (max == currentNumber)
                return Random.Range(min, max);
            else
            {
                if (Random.Range(0, 2) == 0)
                    return Random.Range(min, currentNumber);
                else
                    return Random.Range(currentNumber + 1, max + 1);
            }
        }
    }
}
