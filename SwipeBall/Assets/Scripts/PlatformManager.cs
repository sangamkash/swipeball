﻿using System;
using System.Collections;
using System.Collections.Generic;
using SwipeBall.Constants;
using UnityEngine;
using SwipeBall.Entity;
using UnityEngine.Serialization;

namespace SwipeBall.Entity.Manager
{
    public struct TouchData
    {
        public bool touch;
        public int touchId;
        public Vector2 startTouchPosition;

        public TouchData(int id,bool touch,Vector2 position)
        {
            this.touch = touch;
            touchId = id;
            startTouchPosition = position;
        }
    }
public class PlatformManager : MonoBehaviour
{
    [SerializeField] private Camera mainCamera;
    [SerializeField] private Platform platformPrefab;
    private Platform[] _platforms;
    private int _platformIndex;
    private Vector3 _startPos, _endPos;
    private TouchData[] _touchDatas;
    private TouchData _fingureId1;
    private TouchData _fingureId2;
    public PlatformManager(Platform[] platforms)
    {
        _platforms = platforms;
    }

    private void Start()
    {
        _platforms = new Platform[GameConstants.MaxPlatformCount];
        for (var i = 0; i < GameConstants.MaxPlatformCount; i++)
        {
            _platforms[i] = Instantiate(platformPrefab.gameObject).GetComponent<Platform>();
            _platforms[i].Init();
        }

        _touchDatas = new TouchData[GameConstants.MaxTouchSupport];
        for (int i = 0; i < GameConstants.MaxTouchSupport; i++)
        {
            _touchDatas[i]= new TouchData(-1,false,Vector2.zero);
        }
    }

    private void Update()
    {
        if(Time.timeScale == 0)
            return;
#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {
            _startPos = Input.mousePosition;
        }
        else if(Input.GetMouseButtonUp(0))
        {
            _endPos = Input.mousePosition;
            SpawnPlaform(_startPos, _endPos);
        }
#else
        if (Input.touchCount > 0)
        {
            for (int i = 0; i < Input.touchCount; i++)
            {
                var touch = Input.GetTouch(i);
                if (touch.phase == TouchPhase.Began)
                {
                    for (int j = 0; j < _touchDatas.Length; j++)
                    {
                            
                        if (_touchDatas[j].touch == false)
                        {
#if DEBUG_LOG
                            Debug.Log($"Start::  StartPos {touch.position} touch id {touch.fingerId} ");
#endif
                            _touchDatas[j] = new TouchData(touch.fingerId, true,touch.position);
                            break;
                        }
                    }
                   
                }

                if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
                {
                    for (int j = 0; j < _touchDatas.Length; j++)
                    {
                        if (_touchDatas[j].touchId == touch.fingerId && _touchDatas[j].touch == true)
                        {
#if DEBUG_LOG
                            Debug.Log($"End::  StartPos {touch.position} touch id {touch.fingerId} ");
#endif
                            SpawnPlaform( _touchDatas[j].startTouchPosition, touch.position);
                            _touchDatas[j] = new TouchData(-1, false,Vector2.zero);
                        }
                    }
                }
           
            }
        }
#endif
    }

    private void SpawnPlaform(Vector2 startPos,Vector2 endPos)
    {
        var platform = GetPlatform();
        #if DEBUG_LOG
        Debug.Log($"StartPos {startPos}  endPos {endPos}");
        #endif
        var worldStartPos = Vector3.ProjectOnPlane(mainCamera.ScreenToWorldPoint(startPos), Vector3.forward);
        var worldEndPos = Vector3.ProjectOnPlane(mainCamera.ScreenToWorldPoint(endPos), Vector3.forward);
        var dir = (worldEndPos - worldStartPos).normalized;
        var rot =Vector3.forward * Mathf.Atan2( dir.y,dir.x)*Mathf.Rad2Deg;
        platform.Activate(worldStartPos, rot);
    }
    
    private Platform GetPlatform()
    {
        _platformIndex++;
        if (_platformIndex >= _platforms.Length)
            _platformIndex = 0;
        var obj=_platforms[_platformIndex];
        return obj;
    }
}
}
