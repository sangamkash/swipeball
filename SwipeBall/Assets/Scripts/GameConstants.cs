﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SwipeBall.Constants
{
    public class GameConstants 
    {
        public const int MaxPlatformCount = 4;
        public const float PlatformLife = 2f;

        public const int MaxTouchSupport = 3;
        //Tags
        public const string Tag_Platform = "Platform";
        public const string Tag_Ball = "Ball";
        
        //sceneName
        public const string Scene_Start = "Start";
    }
}

namespace SwipeBall
{
    public static class Utility
    {
        public static bool IsOutOfScreenIngoreUp(Vector3 position,Camera mainCamera)
        {
            var viewPort = mainCamera.WorldToViewportPoint(position);
            return viewPort.x > 1f || viewPort.x < 0;
        }
    }
}