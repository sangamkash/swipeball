﻿using System.Collections;
using System.Collections.Generic;
using SwipeBall.Constants;
using UnityEngine;
using UnityEngine.Serialization;

namespace SwipeBall.Entity
{
    public class Platform : MonoBehaviour
    {
        [SerializeField] private Color startColor;
        [SerializeField] private Color endColor;
        [SerializeField] private SpriteRenderer platfromRenderer;
        [SerializeField] private SpriteRenderer broderRenderer;
        private GameObject _thisGameObject;
        private Transform _thisTransform;
        public void Init()
        {
            _thisGameObject = gameObject;
            _thisTransform = transform;
        }
        
        public void Activate(Vector3 position ,Vector3 eulerAngles)
        {
            _thisTransform.position = position;
            _thisTransform.eulerAngles = eulerAngles;
            _thisGameObject.SetActive(true);
            StartCoroutine(LifeCounter(GameConstants.PlatformLife));
            StartCoroutine(StartBlinkWarning(GameConstants.PlatformLife));
        }

        private IEnumerator LifeCounter(float lifeSpan)
        {
            platfromRenderer.color = startColor;
            var endTime = Time.time + lifeSpan;
            while (Time.time<= endTime)
            {
                var lerpvalue =1f- Mathf.Clamp01((endTime - Time.time) / lifeSpan);
                platfromRenderer.color = Color.Lerp(startColor, endColor, lerpvalue);
                yield return new WaitForEndOfFrame();
            }
            _thisGameObject.SetActive(false);
            StopAllCoroutines();
        }

        private IEnumerator StartBlinkWarning(float lifeSpan)
        {
            broderRenderer.color=Color.white;
            yield return new WaitForSeconds(lifeSpan - 0.6f);
            RepeatBlink();
        }
        
        private void RepeatBlink()
        {
            StartCoroutine(BlinkRepeat());
        }
        
        private IEnumerator BlinkRepeat()
        {
            broderRenderer.color = Color.black;
            yield return new WaitForSeconds(0.1f);
            broderRenderer.color = Color.white;
            yield return new WaitForSeconds(0.1f);
            RepeatBlink();
        }
    }
}